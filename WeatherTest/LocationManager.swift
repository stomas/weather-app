//
//  LocationManager.swift
//  WeatherTest
//
//  Created by rt on 2017-06-09.
//  Copyright © 2017 rt. All rights reserved.
//

import Foundation
import CoreLocation

protocol TSlocationDelegate : class {
    func permissionWasChoosed(answer:Bool)
}

public class LocationManager : NSObject, CLLocationManagerDelegate {
    
    private var latitude:String;
    private var longitude:String;
    var locationManager: CLLocationManager!
    weak var delegate:TSlocationDelegate?
    
    override init() {
        //Initializing lat and long
        self.latitude = ""
        self.longitude = ""
        
        super.init()
        
        locationManager = CLLocationManager()
    }
    
    /*
     * Checks if location service is enabled
     */
    
    func canGetLocation() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            }
        } else {
            return false
        }
    }
    
    
    /*
     * Requests permission for location
     */
    
    func requestPermission() {
        //Getting permissions
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()   
        }
    }
    
    
    
    /*
     * Getting latitude
     */
    func getLatitude() -> String{
        let locValue:CLLocationCoordinate2D = locationManager.location!.coordinate
        return String(format:"%f", locValue.latitude)
    }
    
    /*
     * Getting longitude
     */
    func getLongitude() -> String{
        let locValue:CLLocationCoordinate2D = locationManager.location!.coordinate
        return String(format:"%f", locValue.longitude)
    }
    
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse){
            self.delegate?.permissionWasChoosed(answer: true)
        } else {
            self.delegate?.permissionWasChoosed(answer: false)
        }
    }
}
