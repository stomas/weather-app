//
//  WeatherManager.swift
//  WeatherTest
//
//  Created by rt on 2017-06-09.
//  Copyright © 2017 rt. All rights reserved.
//

import Foundation


class WeatherManager : NSObject {
    
    enum JSONError: String, Error {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
    
    var APIEndpoint:String = "http://api.openweathermap.org/data/2.5/weather?appid=aa264960389ee3baa393113af2d24555&units=imperial"
    
    var currentTemperature:Double = 0
    var minTemperature:Double = 0
    var maxTemperature:Double = 0
    var weatherCondition: String = ""//Is it cloudy or clean or snow...
    
    override init(){
        super.init()
    }
    
    
    func getWeather(lat:String, long:String, finished: @escaping () -> Void){
            getJSONFromAPI(lat: lat, long: long, finished: { json in
                let mainJson:NSDictionary = json.value(forKey: "main") as! NSDictionary
                self.currentTemperature = mainJson.value(forKey: "temp") as! Double
                self.minTemperature = mainJson.value(forKey: "temp_min") as! Double
                self.maxTemperature = mainJson.value(forKey: "temp_max") as! Double
                
                let weatherJson:NSDictionary = (json.value(forKey: "weather") as! NSArray).firstObject as! NSDictionary
                self.weatherCondition = self.getConditionFromWeatherId(weatherId: weatherJson.value(forKey: "id") as! Int)
                finished()
            })
    }
    
    //Get condition from id
    private func getConditionFromWeatherId(weatherId:Int) -> String{
        if(weatherId >= 200 && weatherId < 300){
            return "Thunderstorm"
        } else if(weatherId >= 300 && weatherId < 400){
            return "Drizzle"
        } else if(weatherId >= 500 && weatherId < 600){
            return "Rain"
        } else if(weatherId >= 600 && weatherId < 700){
            return "Snow"
        } else if(weatherId >= 700 && weatherId < 800){
            return "Atmosphere"
        } else if(weatherId == 800) {
            return "Clear"
        } else if(weatherId >= 801 && weatherId < 900){
            return "Clouds"
        } else {
            return "Clouds3"
        }
    }
    
    
    
    //Getting JSON from endpoint
    private func getJSONFromAPI(lat:String, long:String, finished: @escaping (_ withJson:NSDictionary) -> Void){
        let urlPath: String = concatUrl(url: self.APIEndpoint, lat: lat, long: long)
        let url: NSURL = NSURL(string: urlPath)!
        let request: NSURLRequest = NSURLRequest(url: url as URL)
        
        let session = Foundation.URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            do {
                guard let data = data else {
                    throw JSONError.NoData
                }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                    throw JSONError.ConversionFailed
                }
                finished(json)
            } catch let error as JSONError {
                print(error.rawValue)
            } catch let error as NSError {
                print(error.debugDescription)
            }
        })
        task.resume()
    }
    
    //Concat URL for endpoint with location
    private func concatUrl(url:String, lat:String, long:String) -> String {
        return url + "&lat=" + lat + "&lon=" + long
    }

}
