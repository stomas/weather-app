//
//  ViewController.swift
//  WeatherTest
//
//  Created by rt on 2017-06-09.
//  Copyright © 2017 rt. All rights reserved.
//

import UIKit

class ViewController: UIViewController, TSlocationDelegate {

    var weatherManager:WeatherManager = WeatherManager()
    var locationManager: LocationManager = LocationManager()
    
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var minTemperatureLabel: UILabel!
    @IBOutlet weak var highTemperatureLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.weatherManager = WeatherManager();
        
        self.locationManager.delegate = self
        
        getWeather()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func refreshButtonClicked(_ sender: Any) {        
        self.getWeather()
    }
    
    func getWeather(){
        if(locationManager.canGetLocation()){
            DispatchQueue.global(qos: .userInitiated).async {
                self.weatherManager.getWeather(lat: self.locationManager.getLatitude(), long: self.locationManager.getLongitude(), finished: {
                    DispatchQueue.main.async {
                        self.changeMaxTemperature(newTemp: self.weatherManager.maxTemperature)
                        self.changeMinTemperature(newTemp: self.weatherManager.minTemperature)
                        self.changeCurrentTemperature(newTemp: self.weatherManager.currentTemperature)
                        self.changeCurrentImage(newImageName: self.weatherManager.weatherCondition)
                    }
                })
            }
        } else {
            locationManager.requestPermission()
        }
    }

    /*
    * Changing values
    */
    
    func changeCurrentTemperature(newTemp:Double){
        UIView.transition(with: self.currentTemperatureLabel, duration: 1, options: .transitionCrossDissolve, animations: {self.currentTemperatureLabel.text = String(newTemp) + "°"; self.currentTemperatureLabel.isHidden = false}, completion: nil)
    }
    
    func changeMinTemperature(newTemp:Double){
        UIView.transition(with: self.minTemperatureLabel, duration: 1, options: .transitionCrossDissolve, animations: {self.minTemperatureLabel.text = "Low: " + String(newTemp) + "°"; self.minTemperatureLabel.isHidden = false}, completion: nil)

    }
    
    func changeMaxTemperature(newTemp:Double){
        UIView.transition(with: self.highTemperatureLabel, duration: 1, options: .transitionCrossDissolve, animations: {self.highTemperatureLabel.text = "High: " + String(newTemp) + "°"; self.highTemperatureLabel.isHidden = false}, completion: nil)
    }
    
    func changeCurrentImage(newImageName:String){
        let newImage:UIImage = UIImage(named: newImageName)!
        
        UIView.transition(with: self.weatherImage, duration: 1, options: .transitionCrossDissolve, animations: {self.weatherImage.image = newImage}, completion: nil)
    }
    
    

    /*
    * Delegate method from TSlocationDelegate
    */
    func permissionWasChoosed(answer: Bool) {
        if(answer){
            weatherManager.getWeather(lat: locationManager.getLatitude(), long: locationManager.getLongitude(), finished: {})
        }
        
    }
}

